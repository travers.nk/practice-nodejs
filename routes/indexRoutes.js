/**
 * Created by nataly on 20.11.17.
 */
const userRoutes = require('./userRoutes');
const router = require('express').Router();

router.use('/user', userRoutes);

module.exports = router;
