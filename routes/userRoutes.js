/**
 * Created by nataly on 20.11.17.
 */
const router = require('express').Router();
const userController = require('../controllers/UserController');

router.route('/')
    .get(userController.index);

router.route('/user/:nick')
    .get(userController.detail);

module.exports = router;