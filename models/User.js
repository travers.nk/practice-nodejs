/**
 * Created by nataly on 20.11.17.
 */
const fs = require('fs');
const _ = require('lodash');
const path = require('path');

class User {

    getUsers(){
        const users = [];
        const fileName = path.resolve(__dirname, 'users') + '/users.json';
        try {
            let data = fs.readFileSync(fileName, {encoding: 'utf8'});
            data  = data.split(',');
            _.forEach(JSON.parse(data), function(value) {
                users.push(value)
            });
        }
        catch (err){
            console.log(err)
        }
        return users;
    }

    getUserByNick(nick){
        const fileName = path.resolve(__dirname, 'users') + '/' + nick + '.json';
        let data =  {};
        try {
            data = JSON.parse(fs.readFileSync(fileName, {encoding: 'utf8'}));
        }
        catch(err){
            console.log(err)
        }
        return data;
    }
}

module.exports = new User ();