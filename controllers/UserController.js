/**
 * Created by nataly on 20.11.17.
 */
const _ = require('lodash');
const user = require('../models/User');

class UserController {

     index(req, res){
        const users = user.getUsers();
        if (users.length > 0){
            res.render('index', {users});
        } else res.send('empty  list of users');
     }

    detail(req, res){
        const userDetail = user.getUserByNick(req.params.nick);
        if (!_.isEmpty(userDetail)){
            res.render('detail', {userDetail});
        } else res.send('no data about user');
    }
}

module.exports = new UserController();